Changelog
=========

Version 20.9.0
--------------

* Released on PyPI.


Version 20.2.0
--------------

* Free-before-use allacation bug fixed.


Version 1.0
-----------

* lti_dos function added
