from pathlib import Path
from cffi import FFI
ffibuilder = FFI()

ffibuilder.cdef(Path('_ase_ext.h').read_text())

ffibuilder.set_source('_ase_ext',
                      '#include "_ase_ext.h"\n',
                      sources=[path for path in Path().glob('*.c')
                               if path.name != '_ase_ext.c'])

ffibuilder.emit_c_code('_ase_ext.c')
